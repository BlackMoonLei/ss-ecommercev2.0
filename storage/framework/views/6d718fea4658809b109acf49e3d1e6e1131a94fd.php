<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
  <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Our Products
        <!--<small>Example 2.0</small>-->
      </h1>
      <!--<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Layout</a></li>
        <li class="active">Top Navigation</li>
      </ol>-->
    </section>
<section class="content">
  <div class="container-fluid">
            <?php foreach($products as $product): ?>
            <article class="col-sm-4">
                <img alt="" src="<?php echo e(asset('dist/img/prod1.jpg')); ?>" class="img-responsive">
              <section class="boxContent">
                <h3><?php echo e($product->prodDesc); ?></h3>
                <p> <?php echo e($product->prodInfo); ?>.<br />
                  <a href="<?php echo e(url('/500')); ?>" class="moreLink">&rarr; Demo</a>
                   <a href="<?php echo e(action('ssrController@index', $product->id)); ?>" class="moreLink">&rarr; Inquire Now</a>
                </p>
              </section>
            </article>
            <?php endforeach; ?>
            <!-- portfolio item -->

            <!-- portfolio item -->
          
              <!-- portfolio item -->
              <!-- portfolio item -->
                <!-- portfolio item -->
              </div>
     </section>
     <!-- /.content -->
   </div>
   <!-- /.container -->
 </div><!--<ul class="nav navbar-nav pull-right">
               <li class="primary">
               <?php if(Auth::guest()): ?>
                   <br><a href="<?php echo e(url('/login')); ?>" class="firstLevel hasSubMenu">LOGIN</a></br>
                   </li>
                   <li class="sep"></li>
                   <li class="primary">
                     <br><a href="<?php echo e(url('/register')); ?>">REGISTER</a></br>
                   </li>
               <?php else: ?>
                   <li class="dropdown">
                       <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                           <br><?php echo e(Auth::user()->acctUsername); ?> <span class="caret"></span></br>
                       </a>

                       <ul class="dropdown-menu" role="menu">
                           <li><a href="<?php echo e(url('/logout')); ?>"><i class="fa fa-btn fa-sign-out"></i>LOGOUT</a></li>
                           <li><a href="<?php echo e(url('/edit')); ?>"></i>EDIT PROFILE</a></li>
                       </ul>
                   </li>
               <?php endif; ?>-->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>