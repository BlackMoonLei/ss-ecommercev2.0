<?php $__env->startSection('content'); ?>

<div class="content-wrapper">
<section class="content-header">
    <h1>
      Service System Request
    <small>Fill out those boxes completely</small>
    </h1>

  </section>

 <section class="content">
  <div class="row">
        <!-- left column -->

        <div class="col-md-10 col-md-offset-1">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Request your system</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->





            <form action="<?php echo e(action('ssrController@insert'), $id); ?>" method="post">
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                <div class="box-body">
                  <div class="form-group">

                    <label for="name">Name</label>
                       <input type="text" class="form-control" name="ssrName">
                       <p style="color:red;"><?php echo e($errors->first('ssrName')); ?></p>
                  </div>
                  <div class="form-group">

                    <label for="contact">Contact</label>
                        <input type="text"  class="form-control" name="ssrContact">
                          <p style="color:red;"><?php echo e($errors->first('ssrContact')); ?></p>
                  </div>
                  <div class="form-group">

                    <label for="location">Location</label>
                       <input type="text" class="form-control" name="ssrLocation">
                       <p style="color:red;"><?php echo e($errors->first('ssrLocation')); ?></p>
                  </div>
                  <div class="form-group">

                    <label for="problem">Problem</label>
                         <textarea rows="8" cols="100" class="form-control" name="ssrProblem"></textarea>
                           <p style="color:red;"><?php echo e($errors->first('ssrProblem')); ?></p>
                  </div>
                  <div class="form-group">
                    <label for="Service Request">Service Request</label>
                       <textarea rows="8" cols="100" class="form-control" name="ssrServicereq"></textarea>
                         <p style="color:red;"><?php echo e($errors->first('ssrServicereq')); ?></p>
                  </div>
                    <div class="form-group">
                    <label for="Product Type">Product Type</label>
                       <?php echo Form::select('product', $products,$id ); ?>


                  </div>

                     <div class="form-group">
                    <label for="Attachmemt">Attachment</label>
                       <?php echo Form::file('file'); ?>

                  </div>
         
                <br/>
                <input type="hidden" name="userid" value="<?php echo e(Auth::user()->acctUsername); ?>">
                <input type="hidden" name="id" value="<?php echo e(Auth::user()->id); ?>">
                <input type="hidden" name="prod" value="<?php echo e($id); ?>">
                <input type="hidden" name="actType" value="Submitted SSR Form">
                        <div class="box-footer">
                <input type="submit" class="btn btn-primary" value="Submit">
                    </div>


            </form>
          </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>