<?php $__env->startSection('content'); ?>
  <div class="content-wrapper">
    <section class="content-header">
        <h1>
          Edit <?php echo e($user->acctName); ?>'s Profile</h2>
        <small>Fill out those boxes completely</small>
        </h1>

      </section>

     <section class="content">
       <div class="row">
             <!-- left column -->

             <div class="col-md-10 col-md-offset-1">
               <!-- general form elements -->
               <div class="box box-primary">


<?php echo Form::model($user,
  [
  'method' => 'patch',
  'route' => ['userupdate', $user->id],
  'class' => 'form-group',
  ]);; ?>


  <div class="form-group">
     <div class="col-md-3">
      <?php echo Form::label('name', 'Name:'); ?>

     <?php echo Form::text('name', $user->acctName, ['class' => 'form-control']); ?>


   </div>
  </div>

  <div class="form-group">
 <div class="col-md-3">

    <?php echo Form::label('username', 'Username:'); ?>

     <?php echo Form::text('username', $user->acctUsername, ['class' => 'form-control']); ?>

   </div>
  </div>
  <div class="form-group">
<div class="col-md-3">
  <?php echo Form::label('contact', 'Contact No:'); ?>

     <?php echo Form::text('contact', $user->acctContact, ['class' => 'form-control']); ?>

   </div>
  </div>

  <div class="form-group">
<div class="col-md-3">
  <?php echo Form::label('email', 'E-Mail Address:'); ?>

     <?php echo Form::text('email', $user->email, ['class' => 'form-control']); ?>

   </div>
  </div>

  <div class="form-group">
      <div class="col-sm-3">
<br>
        <?php echo Form::submit('Edit', ['class' => 'btn btn-primary  btn-flat ']); ?>

        <?php echo Form::close(); ?>

    </div>
  </div>


</div>


</div>
</section>
</div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>