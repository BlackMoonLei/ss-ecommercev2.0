@extends('layouts.nav')

@section('content')
<div class="content-wrapper">
  <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Our Products
        <!--<small>Example 2.0</small>-->
      </h1>
      <!--<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Layout</a></li>
        <li class="active">Top Navigation</li>
      </ol>-->
    </section>
<section class="content">
   <div id="view" style="width:70%; margin:0 auto;">
            <table id="status" class="table table-hover" cellspacing="0" width="100%">
               <thead>
                <!-- Your heading -->
                     <tr>
                        <th>Product ID</th>
                        <th>Product</th>
                     <tr>
               </thead>
               <tbody>
                   <!-- Your data -->
                    @foreach ($stats as $stat)
                      <tr>
                      <td>{{$stat->status}}</td>
                      <td>{{$stat->statDate}}</td>
                      </tr>
                    @endforeach
                </tbody>
            </table>
       </div>
     </section>
     <!-- /.content -->
   </div>
   <!-- /.container -->
 </div>
@endsection