@extends('layouts.nav')
@section('content')

<div class="content-wrapper">
<section class="content-header">
    <h1>
      Service System Request
    <small>Fill out those boxes completely</small>
    </h1>

  </section>

 <section class="content">
  <div class="row">
        <!-- left column -->

        <div class="col-md-10 col-md-offset-1">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Request your system</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->





            <form action="{{action('ssrController@insert'), $id}}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box-body">
                  <div class="form-group">

                    <label for="name">Name</label>
                       <input type="text" class="form-control" name="ssrName">
                       <p style="color:red;">{{$errors->first('ssrName')}}</p>
                  </div>
                  <div class="form-group">

                    <label for="contact">Contact</label>
                        <input type="text"  class="form-control" name="ssrContact">
                          <p style="color:red;">{{$errors->first('ssrContact')}}</p>
                  </div>
                  <div class="form-group">

                    <label for="location">Location</label>
                       <input type="text" class="form-control" name="ssrLocation">
                       <p style="color:red;">{{$errors->first('ssrLocation')}}</p>
                  </div>
                  <div class="form-group">

                    <label for="problem">Problem</label>
                         <textarea rows="8" cols="100" class="form-control" name="ssrProblem"></textarea>
                           <p style="color:red;">{{$errors->first('ssrProblem')}}</p>
                  </div>
                  <div class="form-group">
                    <label for="Service Request">Service Request</label>
                       <textarea rows="8" cols="100" class="form-control" name="ssrServicereq"></textarea>
                         <p style="color:red;">{{$errors->first('ssrServicereq')}}</p>
                  </div>
                    <div class="form-group">
                    <label for="Product Type">Product Type</label>
                       {!! Form::select('product', $products,$id ) !!}

                  </div>

                     <div class="form-group">
                    <label for="Attachmemt">Attachment</label>
                       {!! Form::file('file') !!}
                  </div>
         
                <br/>
                <input type="hidden" name="userid" value="{{ Auth::user()->acctUsername }}">
                <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                <input type="hidden" name="prod" value="{{$id}}">
                <input type="hidden" name="actType" value="Submitted SSR Form">
                        <div class="box-footer">
                <input type="submit" class="btn btn-primary" value="Submit">
                    </div>


            </form>
          </div>
</div>

@endsection
