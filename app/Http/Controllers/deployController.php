<?php

namespace App\Http\Controllers;


use DB;
use App\Status;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;

class deployController extends Controller
{
	

	 public function index($id)
    {
    	$stats = Status::where('id', $id)->get();
    	return view('deployment', compact('stats', $stats));
    }

}