
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => ['web']], function () {
    Route::get('payPremium/{id}', ['as'=>'payPremium','uses'=>'PaypalController@payPremium']);
    Route::post('getCheckout', ['as'=>'getCheckout','uses'=>'PaypalController@getCheckout']);
    Route::get('getDone', ['as'=>'getDone','uses'=>'PaypalController@getDone']);
    Route::get('getCancel', ['as'=>'getCancel','uses'=>'PaypalController@getCancel']);
});
Route::group(array('before' =>'auth'), function()
{
Route::get('/', 'productController@index');
Route::get('/request/{id}', 'ssrController@index');
Route::get('500', function()
{
    abort(500);
});

Route::get('/messaging', function()
{
    return view('/navigation/messaging');
});
Route::get('/compose', function()
{
    return view('/navigation/compose');
});


Route::get('fbauth/{auth?}', array('as'=>'facebookAuth', 'uses'=>'AuthController@getFacebookLogin'));
Route::get('logout', array('as'=>'logout', 'uses'=>'AuthController@getLoggedOut'));
Route::auth();

Route::get('/home', 'HomeController@index');
Route::post('/ssrform', 'ssrController@insert');
Route::post('/login', 'Auth\AuthController@authenticate');

Route::group(['middleware' => ['web']], function()
{
	Route::get('upload', 'FilesController@upload');
	Route::post('/handleUpload', 'FilesController@handleUpload');
});

Route::get('edit', 'UserController@edit');
Route::patch('update', [
	'as'=>'userupdate',
	'uses'=>'UserController@update'
	]);
});
Route::get('/messages', 'messageController@index');
Route::get('/status/{id}', 'deployController@index');
Route::get('pdfview/{id}',['as'=>'pdfview','uses'=>'PayPalController@pdfview']);